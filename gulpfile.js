var gulp = require('gulp'),
  eslint = require('gulp-eslint'),
  browserSync = require('browser-sync').create();

// Check for js quality issues
gulp.task('lint', function () {
  return gulp.src(['scripts/**/*.js','!node_modules/**'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

// Start a static server
gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: './'
    }
  });
});

// Watch for changes
gulp.task('watch', ['lint', 'serve'], function() {
  gulp.watch('scripts/**/*.js', ['lint']);
  gulp.watch('.eslintrc', ['lint']);
  gulp.watch('index.html', browserSync.reload);
  gulp.watch('scripts/**/*.js', browserSync.reload);
  gulp.watch('styles/**/*.css', browserSync.reload);
});

gulp.task('default', ['watch']);
