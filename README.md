# Connected to the Backbone

> Learning Backbone with TutsPlus [course](http://code.tutsplus.com/courses/connected-to-the-backbone)

## Project Setup

Install Node.js, then install global modules:

```shell
npm install -g gulp bower
```

`cd` to project directory and run:

```shell
npm install && bower install
gulp serve
```

This will start a static server and open the default browser to [http://localhost:3000](http://localhost:3000).

Changes made to source files will automatically be reloaded in the browser, no need to hit browser refresh F5.

## Models

Technique to organize data.

`validate` method is invoked by Backbone when an attribute is set on the model with `{validte: true}` passed.
For example:

```javascript
var Person = Backbone.model.extend({
  defaults: {
    // ...
  },
  validate: function(attrs, options) {
    // ...
  }
});

var p = new Person();
p.set({age: -40}, {validate: true})
```

[Backbone doc for model validate](http://backbonejs.org/#Model-validate)

## Views

In Backbone, Views are not the HTML, rather, they are the representation of a single DOM element.

For example, if want to display a list of People, each Person could have their own PersonView,
represented by a list item dom element, which would be the view's _root element_.

Views can:
* Respond to events
* Have their own methods
* Trigger actions on a model
* Listen for changes to its associated model

By default, when creating a View, Backbone will automatically create an associated root element, which will be a `div`.
This can be overridden by specifying a `tagName` property in the view.

It's also possible to specify `el` property on a view, referencing an existing DOM element on the page.

After instantiating a view, the view object exposes an `el` property which is the DOM element,
and `$el` which is the jQuery wrapped element.

Views have an `initialize` method which is automatically run when view is instantiated. This is like a constructor.

`render` method takes template or root element and groups it together with the associated data.

When a view is instantiated, pass an _instance_ of the model to it:

```javascript
var person = new Person();  // model
var personView = new PersonView({model: person});
```

Now within any view method, have access to the model via `this.model`;

## Collections

Manage multiple instances of models of the same type. Like an array "on steriods".

Like models, collections fire and listen for events. Collection will fire event when:
* item is added
* item is changed or removed or destroyed

When defining a Collection, tell it what model definition to use (not instance like view)l

## Collection View

A view that's responsible for rendering a collection of items.

When instantiating a collection view, pass in an instance of the collection.

## Template Helpers

A better way than embedding script tags in index.html file. Also don't want to inline templates.

## Namespacing

Organizing project to avoid globals.

A simple pattern is to have a single global variable representing the app.

## Dom Events

Views need some way to listen for when events occur, get button click, double click, hover etc.

Add `click` attribute to view and define handlers as functions, for example:

```javascript
App.Views.Task = Backbone.View.extend({
  tagName: 'li',
  events: {
    'click #edit': 'handleTaskClicked'
  },
  handleTaskClicked: function(evt) {
    evt.preventDefault();
    console.log('Task.handleTaskClicked: ' + JSON.stringify(this.model.attributes));
  },
  render: function() {
    // ...
  }
});
```

Above will listen for click on element with id of `edit`, but not searching entire DOM,
only starting from root element of the view tag.

## Custom Events

Also need _custom events_ so that view can listen for any model change and update itself
(unlike Angular, Backbone does not have two-way data binding, so this does not happen automatically).

For example, add lister in view `initialize` method to listen for model change and re-render view:

```javascript
initialize: function() {
  this.model.on('change', this.render, this);
}
```

## Destroying Models

`model.destroy()` removes a model.

`this.$el.remove()` removes a view's root element from the DOM

It's good practice when using Backbone to emit events and respond to them.

## Router

Technique to maintain state of application via URI.

A Backbone application can have multiple routers, but for a simple app, one router is sufficient.

`routes` property defines a hash of uri's to methods.

Empty string `''` is the default route.

Merely defining a router does not trigger any activity. Need to instantiate it AND invoke `Backbone.history.start();`
to start monitoring hash change events by the browser.

### Wildcards and Splats

`:` is wildcard, for example to show any item by id:

```javascript
routes: {
  '': 'index',
  'show/:id': 'show',                    // wildcard
  'download/*filename': 'download',      // splat
  '*other': 'default'                    // unrecognized route
}
```

Backbone automatically captures id from URI and sends it as argument to method that is triggered.

Recommended approach for route handlers is to simply trigger an event, and let another component handle it.

### Triggering Custom Events

Common pattern is to declare an app variable `vent` like so:

```javascript
var vent = _.extend({}, Backbone.Events);
```

Now `vent` has access to `trigger`, `on`, etc of Backbone.Events.

## Backbone and the Server

Specify a `urlRoot` property on a Model. That will be queried when `fetch()` is called on the model.

```javascript
App.Models.Task = Backbone.Model.extend({
  urlRoot: 'http://localhost:3030/tasks',

  defaults: {
    id: '',
    title: '',
    priority: 3,
    completed: false
  }
});

var task = new App.Models.Task({id:1});
task.fetch().then(
  function success(response) {
    // do something with response, for example instantiate a View
  },
  function error(response) {
    // show error...
  }
);
```

### Update Model

Fetch model instance from server, then change any attributes and save:

```javascript
task.set('title', 'updated from UI');
task.save();
```

`save()` will call PUT if model already had an id, POST otherwise.
In either case, Backbone JSON stringifies the model, and sets that as the payload for the xhr request.

### Delete Model

Fetch modelinstance from server, then call `destroy()` to send DELETE request to server:

```javascript
task.destroy();
```

Calling `destroy()` emits a `destroy` event on model. View can listen to this to remove itself from DOM:

```javascript
initialize: function() {
  this.model.on('destroy', this.remove, this);
}
```

Note that `this.remove` in a Backbone.View is simply a convenience for jQuery remove from DOM `this.$el.remove()`.

### Create Model

```javascript
var task = new App.Models.Task({title: 'FOO'});
task.save().then(
  function success() {
    // task is now saved on server and populated with id generated from server
  },
  function error(respose) {
    // handle save error...
  }
);
```

### Fetching into a Collections

Specify `url` property on collection, instantiate it, then call `fetch()`.

Collection method `create(attrs, [options])` is a convenience for creating a new model,
adding it to collection, and saving to server.

## Putting it all together

Need to separate out components in separate js files rather than everything in one massive `main.js` file.
