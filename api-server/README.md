Simple im-memory implementation of a REST api, with CRUD operations on users and tasks.

Run the api server

```
$ DEBUG=api-server nodemon ./bin/www
```
