var express = require('express');
var router = express.Router(); //eslint-disable-line new-cap
var _ = require('underscore');
var uuid = require('node-uuid');
var moment = require('moment');

// Simple in-memory representation
var tasksList = [
  {
    'id' : '1',
    'title' : 'Groceries',
    'priority': 1,
    'completed' : false,
    'lastUpdated': moment().subtract(6, 'days').format('YYYYMMDD')
  },
  {
    'id' : '2',
    'title' : 'Bank',
    'priority': 2,
    'completed' : false,
    'lastUpdated': moment().subtract(4, 'days').format('YYYYMMDD')
  },
  {
    'id' : '3',
    'title' : 'Cleaning',
    'priority': 3,
    'completed' : true,
    'lastUpdated': moment().subtract(3, 'days').format('YYYYMMDD')
  }
];

router.get('/', function(req, res) {
  res.send(tasksList);
});

// 1 second delay to test UI loading indicator
router.get('/:id', function(req, res) {
  setTimeout(function() {
    var taskId = req.params.id;
    var matchingTask = _.find(tasksList, function(task) {
      return taskId === task.id;
    });
    if (matchingTask) {
      res.send(matchingTask);
    } else {
      res.status(404).send({error: {message: 'We did not find a task with id: ' + req.params.id } });
    }
  }, 1000);

});

router.post('/', function(req, res) {
  if (!req.body.title) {
    res.status(400).send({error: {message: 'Title is required'} });
    return;
  }

  var created = {
    id: uuid.v4(),
    title: req.body.title,
    priority: req.body.priority || 3,
    completed: req.body.completed,
    lastUpdated: moment().format('YYYYMMDD')
  };
  tasksList.push(created);

  res.status(201).send(created);
});

router.put('/:id', function(req, res) {
  var taskId = req.params.id;
  var matchingTask;

  if (!req.body.title) {
    res.status(400).send({error: {message: 'Title is required'} });
    return;
  }

  for (var i=0; i<tasksList.length; i++) {
    if (tasksList[i].id === taskId) {
      matchingTask = tasksList[i];
      matchingTask.title = req.body.title;
      matchingTask.priority = req.body.priority;
      matchingTask.completed = req.body.completed;
      matchingTask.lastUpdated = moment().format('YYYYMMDD');
    }
  }

  if (matchingTask) {
    res.send(matchingTask);
  } else {
    res.status(404).send({error: {message: 'We did not find a task with id: ' + req.params.id } });
  }
});

router.delete('/:id', function(req, res) {
  var taskId = req.params.id,
    matchingTask,
    matchingTaskIndex;

  for (var i=0; i<tasksList.length; i++) {
    if (tasksList[i].id === taskId) {
      matchingTask = tasksList[i];
      matchingTaskIndex = i;
      break;
    }
  }

  if (!matchingTask) {
    res.status(404).send({error: {message: 'We did not find a task with id: ' + req.params.id } });
  } else {
    tasksList.splice(matchingTaskIndex, 1);
    res.status(202).send();
  }

});

module.exports = router;
