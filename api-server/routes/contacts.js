var express = require('express');
var router = express.Router(); //eslint-disable-line new-cap
var _ = require('underscore');
var uuid = require('node-uuid');
var moment = require('moment');

// Simple in-memory representation
var contactsList = [
  {
    'id' : '1',
    'first' : 'Joe',
    'last' : 'Schmoe',
    'email' : 'joe.schmoe@test.com',
    'description' : 'friend',
    'lastUpdated': moment().subtract(6, 'days').format('YYYYMMDD')
  },
  {
    'id' : '2',
    'first' : 'Jane',
    'last' : 'Smith',
    'email' : 'jane.smith@test.com',
    'description' : 'teacher',
    'lastUpdated': moment().subtract(4, 'days').format('YYYYMMDD')
  },
  {
    'id' : '3',
    'first' : 'Bob',
    'last' : 'Jones',
    'email' : 'bob.jones@test.com',
    'description' : 'plumber',
    'lastUpdated': moment().subtract(3, 'days').format('YYYYMMDD')
  }
];

var isTaken = function(email, contactList) {
  return _.some(contactList, {email: email});
};

router.get('/', function(req, res) {
  res.send(contactsList);
});

// 1 second delay to test UI loading indicator
router.get('/:id', function(req, res) {
  setTimeout(function() {
    var contactId = req.params.id;
    var matchingContact = _.find(contactsList, function(contact) {
      return contactId === contact.id;
    });
    if (matchingContact) {
      res.send(matchingContact);
    } else {
      res.send(404, {error: {message: 'We did not find a contact with id: ' + req.params.id } });
    }
  }, 1000);

});

router.post('/', function(req, res) {
  var email = req.body.email;

  if (!email) {
    res.send(400, {error: {message: 'Email is required to create a new contact.'} });
    return;
  }

  if (isTaken(email, contactsList)) {
    res.send(400, {error: {message: 'Email ' + email + ' is already in use.'} });
    return;
  }

  var created = {
    id: uuid.v4(),
    email: email,
    first: req.body.first,
    last: req.body.last,
    description: req.body.description,
    lastUpdated: moment().format('YYYYMMDD')
  };
  contactsList.push(created);

  res.status(201).send(created);
});

router.put('/:id', function(req, res) {
  var contactId = req.params.id;
  var matchingContact;

  if (!req.body.email) {
    res.status(400).send({error: {message: 'Email is required'} });
    return;
  }

  for (var i=0; i<contactsList.length; i++) {
    if (contactsList[i].id === contactId) {
      matchingContact = contactsList[i];
      matchingContact.email = req.body.email;
      matchingContact.first = req.body.first;
      matchingContact.last = req.body.last;
      matchingContact.description = req.body.description;
      matchingContact.lastUpdated = moment().format('YYYYMMDD');
    }
  }

  if (matchingContact) {
    res.send(matchingContact);
  } else {
    res.status(404).send({error: {message: 'We did not find a contact with id: ' + req.params.id } });
  }
});

router.delete('/:id', function(req, res) {
  var contactId = req.params.id,
    matchingContact,
    matchingContactIndex;

  for (var i=0; i<contactsList.length; i++) {
    if (contactsList[i].id === contactId) {
      matchingContact = contactsList[i];
      matchingContactIndex = i;
      break;
    }
  }

  if (!matchingContact) {
    res.status(404).send({error: {message: 'We did not find a contact with id: ' + req.params.id } });
  } else {
    contactsList.splice(matchingContactIndex, 1);
    // send empty JSON response, otherwise jQuery fails on parse error
    // which causes client to process this as error instead of success
    res.status(202).send({});
  }

});

module.exports = router;
