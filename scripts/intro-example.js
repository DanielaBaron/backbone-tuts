(function() {

  window.App = {
    Models: {},
    Collections: {},
    Views: {}
  };

  App.Models.Person = Backbone.Model.extend({
    defaults: {
      firstName: 'John',
      lastName: 'Doe',
      age: 30,
      occupation: 'worker'
    },

    validate: function(attrs, options) {
      if (attrs.age < 0) {
        return 'Age must be positive';
      }

      if (!attrs.name) {
        return 'Name cannot be empty';
      }
    }
  });

  App.Collections.People = Backbone.Collection.extend({
    model: App.Models.Person
  });

  // Person view
  App.Views.PersonView = Backbone.View.extend({
    tagName: 'li',
    className: 'person',

    template: templateHelper('personTemplate'),

    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
  });

  // People view
  App.Views.PeopleView = Backbone.View.extend({
    tagName: 'ul',
    className: 'people',

    render: function() {
      this.collection.each(function(personModel) {
        var personView = new App.Views.PersonView({model: personModel});
        this.$el.append(personView.render().el);
      }, this);
      return this;
    }
  });

  // Instantiate collection with array of objects that Backbone will convert to Person models
  var people = new App.Collections.People([
    {firstName: 'Jeff', lastName: 'Way', age: 27},
    {firstName: 'John', lastName: 'Doe', age: 50, occupation: 'Web Designer'},
    {firstName: 'Sally', lastName: 'Smith', age: 29, occupation: 'Graphic Designer'}
  ]);

  var peopleView = new App.Views.PeopleView({collection: people});

  // Place the rendered view into the DOM
  $(function() {
    $(document.body).html(peopleView.render().el);
  });

})();
