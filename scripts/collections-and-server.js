(function() {

  window.App = { Models: {}, Views: {}, Collections: {} };

  // Define the Task Model, backed by server API
  App.Models.Task = Backbone.Model.extend({
    urlRoot: 'http://localhost:3030/tasks',
    defaults: {
      title: '',
      priority: 3,
      completed: false
    }
  });

  // Define the Task Collection, backed by server API
  App.Collections.Tasks = Backbone.Collection.extend({
    url: 'http://localhost:3030/tasks',
    model: App.Models.Task
  });

  // Define the Task View
  App.Views.Task = Backbone.View.extend({
    tagName: 'li',
    initialize: function() {
      this.model.on('destroy', this.remove, this);
    },
    render: function() {
      this.$el.html(this.model.get('title'));
      return this;
    }
  });

  // Define the Tasks View
  App.Views.Tasks = Backbone.View.extend({
    tagName: 'ul',
    initialize: function() {
      this.collection.on('add', this.addOne, this);
    },
    render: function() {
      this.$el.empty();
      this.collection.each(this.addOne, this);
      return this;
    },
    addOne: function(task) {
      var taskView = new App.Views.Task({model: task});
      taskView.render();
      this.$el.append(taskView.render().el);
    }
  });

  // Instantiate the tasks collection
  var tasks = new App.Collections.Tasks();

  // Populate tasks collection from server
  tasks.fetch().then(
    function success() {

      // Instantiate tasks collection view, passing in instance of tasks collection
      var tasksView = new App.Views.Tasks({collection: tasks});

      // Place the rendered view into the DOM
      $('.container').html(tasksView.render().el);

      // Create new model, add to collection and save to server - all in one line
      tasks.create({title: 'Do it now'});
    },
    function error(response) {
      console.error(response.responseText);
    }

  );

  // expose tasks as global so we can play with it in devtools
  window.tasks = tasks;

})();
