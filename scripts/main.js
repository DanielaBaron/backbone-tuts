(function() {

  // Namespace the application and expose as global
  window.ContactsApp = {
    Models: {},
    Collections: {},
    Views: {},
    Router: {}
  };

  // Application pubsub exposed as global
  window.vent = _.extend({}, Backbone.Events);

})();
