(function() {

  // Initialize the router
  new ContactsApp.Router();

  // Start recording history
  Backbone.history.start();

  // Instantiate Contacts Collection and persist it on App object
  ContactsApp.contacts = new ContactsApp.Collections.Contacts();

  // Populate Contacts Collection from server and handle success or error
  ContactsApp.contacts.fetch().then(

    function success() {
      // Instantiate App View, injecting the populated Contacts Collection
      new ContactsApp.Views.App( {collection: ContactsApp.contacts } );
    },

    function error(response) {
      // Display error message if xhr failed
      console.error(response.responseText);
    }

  );

})();
