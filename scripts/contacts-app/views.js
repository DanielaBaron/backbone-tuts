/*
|------------------------------------------------------------------------------
| Global App View
|------------------------------------------------------------------------------
*/
ContactsApp.Views.App = Backbone.View.extend({

  // This view attaches itself into the main container element already present in the dom (see index.html)
  el: '#contactsApp',

  initialize: function() {
    // Register listener for show message event
    vent.on('message:show', this.showMessage, this);

    // Instantiate the Add Contact View, passing in populated Contacts collection
    var addContactView = new ContactsApp.Views.AddContact( {collection: ContactsApp.contacts});

    // Render the Add Contact View and append it to this view's root element
    this.$el.append(addContactView.render().el);

    // Instantiate the Contacts View, passing in populated Contacts collection
    var contactsView = new ContactsApp.Views.Contacts({collection: ContactsApp.contacts});

    // Render the Contacts View and append it to this view's root element
    this.$el.append(contactsView.render().el);
  },

  // Instantiate a message model and view
  showMessage: function(message) {
    var messageModel = new ContactsApp.Models.Message({messageText: message.text, messageType: message.type});
    var messageView = new ContactsApp.Views.Message({model: messageModel});
    messageView.render();
  }

});

/*
|------------------------------------------------------------------------------
| Global Message View
|------------------------------------------------------------------------------
*/
ContactsApp.Views.Message = Backbone.View.extend({

  // This view attaches itself into the message container element already present in the dom (see index.html)
  el: '#contactsAppMessage',
  template: templateHelper('messageTemplate'),

  initialize: function() {

  },

  events: {
    'click #clearMessage': 'clearMessage'
  },

  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    this.$el.removeClass('hide');
    this.$el.addClass('show');
    return this;
  },

  clearMessage: function(evt) {
    evt.preventDefault();
    this.$el.removeClass('show');
    this.$el.addClass('hide');
  }

});

/*
|------------------------------------------------------------------------------
| Add Contact View
|------------------------------------------------------------------------------
*/
ContactsApp.Views.AddContact = Backbone.View.extend({
  className: 'add-contact-view',
  template: templateHelper('contactFormTemplate'),

  render: function() {
    this.$el.html(this.template());
    return this;
  },

  // Register event handler to add contact when form is submitted
  events: {
    'submit' : 'addContact'
  },

  // Invoked when form is submitted
  addContact: function(evt) {
    var self = this;
    evt.preventDefault();

    // Extract contact attributes from form data
    var contactAttributes = {
      first: this.$('#firstName').val(),  // shorthand: this.$el.find('#firstName')
      last: this.$('#lastName').val(),
      email: this.$('#email').val(),
      description: this.$('#description').val()
    };

    // Define request options including success and error handlers
    var requestOptions = {
      wait: true,   // wait for server success response before adding to collection
      success: function(contactModel) {
        vent.trigger('message:show', {text: 'Contact added successfully.', type: 'success'});
        self.clearForm();
      },
      error: function(contactModel, serverResponse) {
        vent.trigger('message:show', {text: serverResponse.responseJSON.error.message, type: 'error'});
      }
    };

    // Create a new contact from form data, add to collection, and save to server
    // This triggers an 'add' event on the collection, other views can respond to this
    var newContact = this.collection.create(contactAttributes, requestOptions);
    if (!newContact.isValid()) {
      vent.trigger('message:show', {text: newContact.validationError, type: 'error'});
    }
  },

  clearForm: function() {
    this.$('#firstName').val('');
    this.$('#lastName').val('');
    this.$('#email').val('');
    this.$('#description').val('');
  }
});

/*
|------------------------------------------------------------------------------
| Contact View
|------------------------------------------------------------------------------
*/
ContactsApp.Views.Contact = Backbone.View.extend({
  tagName: 'tr',
  template: templateHelper('contactTemplate'),

  initialize: function() {
    this.model.on('destroy', this.unrender, this);
  },

  events: {
    'click .delete': 'deleteContact'
  },

  deleteContact: function(evt) {
    evt.preventDefault();
    
    var self = this;
    var destroyOptions = {
      wait: true,
      success: function(model) { vent.trigger('message:show', {text: 'Contact ' + model.get('email') + ' removed.', type: 'success'}); },
      error: function(model, serverResponse) { vent.trigger('message:show', {text: serverResponse.responseJSON.error.message, type: 'error'}); }
    };

    // makes DELETE request to api server for this model id, and fires 'destroy' event on model,
    // which this view listens for in initialize method
    this.model.destroy(destroyOptions);
  },

  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  },

  unrender: function() {
    this.remove();  // shorthand for this.$el.remove();
  }
});

/*
|------------------------------------------------------------------------------
| All Contacts View
|------------------------------------------------------------------------------
*/
ContactsApp.Views.Contacts = Backbone.View.extend({
  template: templateHelper('allContactsTemplate'),

  initialize: function() {
    // after contact has been saved to server, update the view
    this.collection.on('sync', this.addOne, this);
  },

  render: function() {
    this.$el.html(this.template());
    this.collection.each(this.addOne, this);
    return this;
  },

  addOne: function(contact) {
    var contactView = new ContactsApp.Views.Contact({model: contact});
    this.$el.find('tbody').append(contactView.render().el);
  }
});
