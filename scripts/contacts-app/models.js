ContactsApp.Models.Contact = Backbone.Model.extend({

  urlRoot: 'http://localhost:3030/contacts',

  defaults: {
    first: '',
    last: '',
    email: '',
    description: ''
  },

  // Client side validation rules
  validate: function(attrs) {
    if (! attrs.email) {
      return 'Email is required to create a contact.';
    }
  }

});

ContactsApp.Models.Message = Backbone.Model.extend({

  defaults: {
    messageText: '',
    messageType: 'success'
  }

});
