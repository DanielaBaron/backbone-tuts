(function() {

  // Namespace our app
  window.App = { Models: {}, Collections: {}, Views: {}, Router: {} };

  // Custom events
  var vent = _.extend({}, Backbone.Events);

  // Declare appointment view
  App.Views.AppointmentView = Backbone.View.extend({
    initialize: function() {
      vent.on('appointment:show', this.showAppointment, this);
    },
    showAppointment: function(appointmentId) {
      console.log('AppointmentView.showAppointment: ' + appointmentId);
    }
  });

  // Define a router
  App.Router = Backbone.Router.extend({
    routes: {
      '': 'index',
      'appointment/:id': 'showAppointment',
      'download/*filename': 'download',
      '*other': 'default'
    },

    index: function() {
      console.log('Index view');
    },

    showAppointment: function(appointmentId) {
      vent.trigger('appointment:show', appointmentId);
    },

    download: function(filename) {
      console.log('Show download: ' + filename);
    },

    default: function(other) {
      console.log('Unrecognized route: ' + other);
    }
  });

  // Instantiate the router
  new App.Router();
  Backbone.history.start();

  // Instantiate appointment view
  new App.Views.AppointmentView();

})();
