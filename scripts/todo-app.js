/**
 * A simple task application.
 */
(function() {

  // Namespace
  window.App = { Models: {}, Collections: {}, Views: {} };

  // Define Task Model
  App.Models.Task = Backbone.Model.extend({
    defaults: {
      title: 'Enter a task...',
      priority: 1
    },
    validate: function(attrs) {
      if (! $.trim(attrs.title) ) {
        return 'Title required';
      }
    }
  });

  // Define Task Collection, specifying model type of Task
  App.Collections.Tasks = Backbone.Collection.extend({
    model: App.Models.Task
  });

  // Define Task View
  App.Views.Task = Backbone.View.extend({
    tagName: 'li',
    className: 'task',
    template: templateHelper('taskTemplate'),

    initialize: function() {
      // register listener on model to re-render entire view whenever model changes
      this.model.on('change', this.render, this);
      this.model.on('destroy', this.remove, this);
    },

    events: {
      'click #editTask': 'editTask',
      'click #deleteTask': 'deleteTask'
    },

    editTask: function(evt) {
      evt.preventDefault();
      // we don't have a form yet, so use ugly prompt for now
      var updatedTitle = prompt('Enter task title:', this.model.get('title'));
      // update model, note that updating model does not automatically update the view
      this.model.set({title: updatedTitle}, {validate: true});
    },

    deleteTask: function(evt) {
      evt.preventDefault();
      this.model.destroy();
    },

    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },

    remove: function() {
      // call jQuery method to remove this view's root element
      this.$el.remove();
    }
  });

  // Define Tasks Collection View
  App.Views.Tasks = Backbone.View.extend({
    tagName: 'ul',
    className: 'tasks',

    initialize: function() {
      this.collection.on('add', this.addOne, this);
    },

    render: function() {
      this.collection.each(this.addOne, this);
      return this;
    },

    addOne: function(taskModel) {
      var taskView = new App.Views.Task({model: taskModel});
      this.$el.prepend(taskView.render().el);
    }
  });

  // Define the AddTask view
  App.Views.AddTask = Backbone.View.extend({
    el: '#addTask',

    events: {
      'submit' : 'addTask'
    },

    addTask: function(evt) {
      evt.preventDefault();
      // retrieve user input from DOM
      var newTaskTitle = $(evt.currentTarget).find('input[type=text]').val();
      // create new model (doing here in submit method for now)
      var task = new App.Models.Task({title: newTaskTitle});
      // add task to task collection (does not automatically update view)
      this.collection.add(task);
    }
  });

  // Instantiate the tasks collection.
  // Each entry in array will become a Task model,
  // because Tasks collection is defined to have its model of type Task
  var tasks = new App.Collections.Tasks([
    { title: 'Wash makeup brushes', priority: 3 },
    { title: 'Buy coconut oil at whole foods', priority: 2 },
    { title: 'Drop off paperwork', priority: 1 }
  ]);

  // Instantiate tasks collection view, passing in instance of tasks collection
  var tasksView = new App.Views.Tasks({collection: tasks});

  // Instantiate the add task view (no need to render because its attaching to a dom el already on the page)
  new App.Views.AddTask({collection: tasks});

  // Place the rendered view into the DOM
  $(function() {
    $('.container').html(tasksView.render().el);
  });

})();
